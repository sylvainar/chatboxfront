import agent from 'superagent';
const apiUrl = 'http://afternoon-oasis-70412.herokuapp.com/api/channels';

export const fetchChannels = async () => {
  try {
    const res = await agent.get(apiUrl).query();
    return res.body;
  }
  catch(err) {
    console.error(err);
  }
};

export const postChannels = async (channel) => {
  try {
    // channel : {name: "yolo", ...}
    const res = await agent.post(apiUrl).send(channel);
    return res.body;
  }
  catch(err) {
    console.error(err);
  }
};

export const deleteChannel = async (channel) => {
  try {
    const url = `${apiUrl}/${channel._id}`;
    const res = await agent.delete(url).query();
    return res.body;
  }
  catch(err) {
    console.error(err);
  }
};