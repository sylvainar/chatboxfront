import React, { Component } from 'react';
import ChannelContainer from "./ChannelContainer";

class App extends Component {
  render() {
    return (
      <div>
        <ChannelContainer/>
      </div>
    );
  }
}

export default App;
