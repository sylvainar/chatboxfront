import React from 'react';
import {deleteChannel, fetchChannels, postChannels} from "../repository/channels.repository";
import ChannelList from "./ChannelList";
import './channelContainer.css';
import TextInput from "./TextInput";

export default class ChannelContainer extends React.PureComponent {
  state = {
    channelList: [],
  };

  componentWillMount() {
    this.getList();
  }

  getList = () => {
    fetchChannels()
      .then(result => {
        console.log(result);
        this.setState({
          channelList: result,
        })
      })
  }

  handleNewChannel = (channelName) => {
    const channel = {
      name: channelName,
    };


    postChannels(channel).then(() => {
      this.getList()
    });
  };

  handleDeleteChannel = (channel) => {
    deleteChannel(channel).then(() => {
      this.getList()
    })
  };

  render() {
    return <div className="channelContainer">
      <ChannelList
        channelList={this.state.channelList}
        buttonCallback={this.handleDeleteChannel}
      />
      <TextInput
        callback={this.handleNewChannel}
      />
    </div>;
  }
}